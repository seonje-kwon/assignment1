Install

Copy and connect the repository locally by entering git clone and the repository URL at your command line:

git clone https://seonje-kwon@bitbucket.org/seonje-kwon/d.git.


How To Use

You can write reviews about League of Legends champions and see those reviews.


License

The reason why I chose this license is because it allows others to copy, modify, distribute and perform the work, even for commercial
purposes, all without asking permission.